#LISTA DE VARIABLES Y USO QUE SE LES DA
# opc: Variable que guarda la selección de la opción del user

#PARTE DE OCULTAMIENTO (OPC==1)
# grado: Variable que guarda el número de bits que se ocultarán en el mismo pixel
# nombretexto: Nombre del archivo .txt que guarda el texto a ocultar
# nombreimagen: Nombre de la imagen .png en la que se ocultará el texto
# ioriginal: Imagen original en la que se guardará el texto
# aoriginal: Array original de la imagen original en la que se guardará el texto
# toriginal: Texto original a ocultar
# longt: Longitud del texto original
# tbinario:  Array lista de listas (como una matriz) que guarda los bytes del texto
# inbits: Lista de cadenas de bits que forman los bytes del texto
# textobits: Cadena que contiene todos los bits del texto seguidos (cada ocho bits forman un byte)
# longbits:  Número de bits contenidos en el texto
# altoimagen: Número de píxeles de alto que mide la imagen
# anchoimagen: Número de píxeles de ancho que mide la imagen
# afinal: Array final de la imagen con el texto guardado
# ifinal: Imagen final con el texto guardado
# nombreguardado: Nombre que tendrá la imagen con el texto oculto

#PARTE DE EXTRACCIÓN (OPC==2)
# nombreoculto: Nombre del archivo de imagen que contiene el texto
# nombreclave: Nombre de la imagen que es la clave para la extracción del texto
# imag: Imagen con el texto guardado
# aimag: Array de la imagen con el texto 
# clave: Imagen clave
# aclave: Array de la imagen clave
# grado: Variable que guarda el número de bits que se ocultarán en el mismo pixel
# bitstexto: Número de bits que contiene el texto
# altoimagen: Número de píxeles de alto que mide la imagen
# anchoimagen: Número de píxeles de ancho que mide la imagen
# textodec: Lista que guarda el número decimal asociado a cada carácter del texto
# texto: Cadena con el texto original encontrado en la imagen
# nombrenuevo: Nombre que tendrá el archivo con el texto extraído


# cont: Contador principal para bucles
# cont1: Contador secundario para bucles
# inter: Intermediario principal para bucles
# alfa: Intermediario secundario para bucles

# a: Variable empleada como auxiliar para el empleo de la función que imprime por pantalla el estado del proceso que se está realizando
def pcomplet(v,vmax,t,a):#Función que presenta por pantalla el nivel de completado de las distintas partes del programa
    if (v>vmax*0.2 and a==0):
        print(t, '20%')
        a+=1
    elif (v>vmax*0.4 and a==1):
        print(t, '40%')
        a+=1
    elif (v>vmax*0.6 and a==2):
        print(t, '60%')
        a+=1
    elif (v>vmax*0.8 and a==3):
        print(t, '80%')
        a+=1
    return a

from PIL import Image
from numpy import array
print('Elija una opción')
print('1. Ocultar un mensaje')
print('2. Extraer un mensaje')
print('3. Salir del programa')
print('')
opc=int(input('Introduzca la opción deseada: '))

if (opc==1):
    #grado=int(input('Elija el número de bits a ocultar por pixel (de 1 a 3): '))
    nombretexto=input('Introduzca el nombre del arcihvo que contiene el texto (acabado en .txt y sin tilde): ')
    nombreimagen=input('Introduzca el nombre del archivo de imagen donde se ocultará el texto (acabado en .png): ')    
    grado=3 #Numero de bits que se guardan por pixel de la imagen modificada
            #Se puede modificar el valor a 1, 2 o 3 (este cambio se debe repetir en
            #la parte de OPC==2, que es la de extraccion)
    ioriginal=Image.open(nombreimagen)
    aoriginal=array(ioriginal)
    fileHandle=open(nombretexto,'r')
    toriginal=fileHandle.read()
    fileHandle.close()
    longt=len(toriginal)
    tbinario=[]
    inbits=[]
    tbinario=[0]*longt
    for cont in range (longt):
        tbinario[cont]=[0]*8
        
    a=0
    for cont in range (longt):        
        ####################################
        a=pcomplet(cont,longt,'Lectura del texto completada al ',a)
        ####################################        
        alfa=(bin(ord(toriginal[cont])))
        inbits=inbits+[alfa]
        inter=int(len(inbits[cont]))
                        
        if (inter==8):
            for cont1 in range (6):
                if (inbits[cont][2+cont1]=='1'):
                    tbinario[cont][2+cont1]=1
                elif (inbits[cont][2+cont1]=='0'):
                    tbinario[cont][2+cont1]=0
                tbinario[cont][0]=0
                tbinario[cont][1]=0
            
        if (inter==9):
            for cont1 in range (8):
                if (inbits[cont][1+cont1]=='1'):
                    tbinario[cont][cont1]=1
                elif (inbits[cont][1+cont1]=='0'):
                    tbinario[cont][cont1]=0
                tbinario[cont][0]=0

        elif (inter==10):
            inbits[cont]=inbits[cont][2:10]
            for cont1 in range (8):
                if (inbits[cont][cont1]=='1'):
                    tbinario[cont][cont1]=1
                elif (inbits[cont][cont1]=='0'):
                    tbinario[cont][cont1]=0
        
        textobits=[]
        longbits=len(tbinario)*8
        for cont in range (int(len(tbinario))):#len(tbinario) en teoría debe tener el mismo valor que longt
            for cont1 in range (8):
                inter=tbinario[cont][cont1]
                textobits+=[inter]    
        
    altoimagen=int(len(aoriginal))
    anchoimagen=int(len(aoriginal[0]))
    if (longbits>(altoimagen*anchoimagen*grado)):
        print('La imagen es demasiado pequeña para albergar el texto') #O se le debe poner un grado mas alto
        print('El texto no se ha ocultado')
    else:
        afinal=aoriginal
        cont=0
        a=0
        while (cont<longbits):
            inter=cont//altoimagen
            alfa=cont%altoimagen
            #selecciono el vector afinal[alfa][inter] RECORDAR: array[Y][X]
            ####################################
            a=pcomplet(cont,longbits,'Ocultamiento del texto completado al ',a)
            ####################################
            for cont1 in range (grado):
                if (cont==longbits):
                    break
                afinal[alfa][inter][cont1]+=textobits[cont]
                cont+=1
        nombreguardado=str(int(longbits))+'.png'      
        ifinal=Image.fromarray(afinal)
        ifinal.save(nombreguardado)
        print('DATO IMPORTANTE: El texto contiene ',longbits,' bits')
            
elif (opc==2):
    nombreoculto=input('Introduce el nombre de la imagen que contiene el texto (incluye el .png): ')
    imag=Image.open(nombreoculto)
    aimag=array(imag)
    nombreclave=input('Introduce el nombre de la imagen original (incluye el .png): ')
    clave=Image.open(nombreclave)
    aclave=array(clave)
    #grado=int(input('Elija el número de bits a ocultar por pixel (de 1 a 3): '))
    grado=3 #Numero de bits que se guardan por pixel de la imagen modificada
            #Se puede modificar el valor a 1, 2 o 3 (este cambio se debe repetir en
            #la parte de OPC==1, que es la de ocultacion)
    bitstexto=int(input('Introduzca  el número de bits del texto: '))
    textobits=[]
    altoimagen=int(len(aclave))
    anchoimagen=int(len(aclave[0]))
    cont=0
    a=0
    while (cont<bitstexto):
        inter=cont//altoimagen
        alfa=cont%altoimagen
        for cont1 in range (grado):
            textobits+=[int(aimag[alfa][inter][cont1]-aclave[alfa][inter][cont1])]
            cont+=1
        ####################################
        a=pcomplet(cont,bitstexto,'Extracción del texto completada al ',a)
        ####################################
    textodec=[]
    inter=''
    texto=''
    a=0
    for cont in range  (bitstexto):
        inter+=str(textobits[cont])
        if ((cont+1)%8==0 and cont!=0):
            alfa=int(inter,2)
            textodec+=[alfa]
            inter=''
        ####################################
        a=pcomplet(cont,bitstexto,'Creación del archivo completada al ',a)
        ####################################
    for cont in range (bitstexto//8):
        texto+=str(chr(textodec[cont]))
    nombrenuevo=input('Introduce el nombre del archivo que contendrá el texto extraído (incluye el .txt): ')
    text_file=open(nombrenuevo,'w')
    text_file.write(str(texto))
    text_file.close()  

input('Press enter to continue')


#Made by Criptos
#---------------------------------------
#Sugerencias, mejoras, problemas y dudas
#criptos@openmailbox.org
#GPG: 0xA1C0A8F8